import json
import os
from app.foundation import db, logger
from app.models.Sample import Sample
from test import test_app
from urllib.parse import urlencode

class TestSample(object):
    _id = -1

    @classmethod
    def setup_class(self):
        logger.info('\nsetup_class: %s' % os.path.basename(__file__))
        Sample.query.delete()
        db.session.commit()

    @classmethod
    def teardown_class(self):
        logger.info('\nteardown_class: %s' % os.path.basename(__file__))
        Sample.query.delete()
        db.session.commit()

    def test_unknown(self):
        ret = test_app.get('/api/unknown')
        assert ret.status_code == 200

        ret_json = json.loads(ret.data.decode('utf8'))
        assert ret_json['code'] == 404

    def test_get_ok1(self):
        ret = test_app.get('/api/test')
        assert ret.status_code == 200

        ret_json = json.loads(ret.data.decode('utf8'))
        assert ret_json['code'] == 0
        assert len(ret_json['data']) == 0

    def test_post_fail(self):
        ret = test_app.post('/api/test')
        assert ret.status_code == 200

        ret_json = json.loads(ret.data.decode('utf8'))
        assert ret_json['code'] == -1002

    def test_post_ok(self):
        payload = {
            'data': 'test'
        }
        ret = test_app.post('/api/test', data=payload)
        assert ret.status_code == 200

        ret_json = json.loads(ret.data.decode('utf8'))
        assert ret_json['code'] == 0
        assert ret_json['data']['data'] == 'test'
        TestSample._id = ret_json['data']['id']

    def test_get_ok2(self):
        ret = test_app.get('/api/test')
        assert ret.status_code == 200

        ret_json = json.loads(ret.data.decode('utf8'))
        assert ret_json['code'] == 0
        assert len(ret_json['data']) == 1

    def test_put_fail(self):
        payload = {
            'data': 'test2'
        }
        ret = test_app.put('/api/test/100', data=payload)
        assert ret.status_code == 200

        ret_json = json.loads(ret.data.decode('utf8'))
        assert ret_json['code'] == -1003

    def test_put_ok(self):
        payload = {
            'data': 'test2'
        }
        ret = test_app.put('/api/test/'+str(TestSample._id), data=payload)
        assert ret.status_code == 200

        ret_json = json.loads(ret.data.decode('utf8'))
        assert ret_json['code'] == 0
        assert ret_json['data']['data'] == 'test2'

    def test_get_ok3(self):
        payload = {
            'id': TestSample._id
        }
        ret = test_app.get('/api/test?'+urlencode(payload))
        assert ret.status_code == 200

        ret_json = json.loads(ret.data.decode('utf8'))
        assert ret_json['code'] == 0
        assert ret_json['data']['data'] == 'test2'

    def test_delete_fail(self):
        ret = test_app.delete('/api/test/100')
        assert ret.status_code == 200

        ret_json = json.loads(ret.data.decode('utf8'))
        assert ret_json['code'] == -1003

    def test_delete_ok(self):
        ret = test_app.delete('/api/test/'+str(TestSample._id))
        assert ret.status_code == 200

        ret_json = json.loads(ret.data.decode('utf8'))
        assert ret_json['code'] == 0

    def test_get_ok4(self):
        payload = {
            'id': TestSample._id
        }
        ret = test_app.get('/api/test?'+urlencode(payload))
        assert ret.status_code == 200

        ret_json = json.loads(ret.data.decode('utf8'))
        assert ret_json['code'] == -1003
