class ErrorCode:
    ERROR_UNKNOWN = (1000, '未知錯誤')
    ERROR_DB_ERROR = (1001, '資料庫錯誤')
    ERROR_INVALID_PARAM = (1002, '參數錯誤')
    ERROR_NO_DATA_FOUND = (1003, '查無資料')
