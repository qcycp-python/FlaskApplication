import logging

class Logger(object):
    def __init__(self, name: str) -> None:
        formatter = logging.Formatter('%(asctime)s - [%(pathname)s:%(lineno)s] - %(levelname)s %(message)s ')
        self.__logger = logging.getLogger(name)
        self.__logger.setLevel(logging.DEBUG)

        streamHandler = logging.StreamHandler()
        streamHandler.setFormatter(formatter)
        self.__logger.addHandler(streamHandler)

    def get_logger(self) -> logging.Logger:
        return self.__logger
