from flask import jsonify
from typing import Dict, Tuple
from app.common.error_code import ErrorCode

def success_result(data: Dict = {}, page: Dict = {}) -> jsonify:
    ret = {
        'code': 0,
        'data': data,
        **({'page': page} if page else {})
    }
    return jsonify(ret)


def error_result(error: Tuple = ErrorCode.ERROR_UNKNOWN, message: str = "", data: Dict = {}, with_code: bool = False) -> jsonify:
    code, message = error
    ret = {
        'code': -code,
        'data': data,
        'message': "u%s (error: %d)"%(message, code) if with_code else message
    }
    return jsonify(ret)
