import traceback
from flask import Blueprint, jsonify, request
from app.common.error_code import ErrorCode
from app.common.json_builder import success_result, error_result
from app.foundation import db, logger
from app.models.Sample import Sample

sample_bp = Blueprint('sample_bp', __name__, url_prefix='/api')

@sample_bp.route("/test", methods=['GET'])
def get_method() -> jsonify:
    params = request.args
    try:
        id = params.get('id', None)
    except:
        logger.error(traceback.format_exc())
        return error_result(ErrorCode.ERROR_INVALID_PARAM)

    try:
        if id:
            sample = Sample.query.filter_by(id=id).first()
            if sample:
                return success_result(sample.get_json())
            else:
                return error_result(ErrorCode.ERROR_NO_DATA_FOUND)
        else:
            samples = Sample.query.all()
            ret = [sample.get_json() for sample in samples]
            return success_result(ret)
    except:
        logger.error(traceback.format_exc())
        return error_result(ErrorCode.ERROR_UNKNOWN)

@sample_bp.route("/test", methods=['POST'])
def post_method() -> jsonify:
    params = request.form or request.get_json()
    try:
        data = params.get('data', None)
    except:
        logger.error(traceback.format_exc())
        return error_result(ErrorCode.ERROR_INVALID_PARAM)

    try:
        sample = Sample(data=data)
        db.session.add(sample)
        db.session.commit()
        return success_result(sample.get_json())
    except:
        db.session.rollback()
        logger.error(traceback.format_exc())
        return error_result(ErrorCode.ERROR_DB_ERROR)

@sample_bp.route("/test/<int:uid>", methods=['PUT'])
def put_method(uid) -> jsonify:
    params = request.form or request.get_json()
    try:
        data = params.get('data', None)
    except:
        logger.error(traceback.format_exc())
        return error_result(ErrorCode.ERROR_INVALID_PARAM)

    sample = Sample.query.filter_by(id=uid).first()
    if not sample:
        return error_result(ErrorCode.ERROR_NO_DATA_FOUND)

    try:
        if data:
            sample.data = data
        db.session.commit()
        return success_result(sample.get_json())
    except:
        db.session.rollback()
        logger.error(traceback.format_exc())
        return error_result(ErrorCode.ERROR_DB_ERROR)

@sample_bp.route("/test/<int:uid>", methods=['DELETE'])
def delete_method(uid) -> jsonify:
    params = request.form or request.get_json()

    sample = Sample.query.filter_by(id=uid).first()
    if not sample:
        return error_result(ErrorCode.ERROR_NO_DATA_FOUND)

    try:
        db.session.delete(sample)
        db.session.commit()
        return success_result()
    except:
        db.session.rollback()
        logger.error(traceback.format_exc())
        return error_result(ErrorCode.ERROR_DB_ERROR)
