from typing import Any, Dict
from app.foundation import db
from app.models.Base import Base

class Sample(Base):
    __tablename__ = 'Sample'

    data = db.Column(db.String(64))

    def __init__(self, **kwargs: Any) -> None:
        for k, v in kwargs.items():
            setattr(self, k, v)

    def get_json(self) -> Dict:
        info = {
            'id': self.id,
            'data': self.data
        }

        return info
