import os
import time
from flask import Blueprint, Flask, g, request, jsonify
from flask_cors import CORS
from typing import Any, List
from werkzeug.exceptions import HTTPException
from app import views
from app.common.db_utils import check_database, create_database
from app.foundation import db, logger

def create_app() -> Flask:
    app = Flask(__name__)
    app.config.from_object('config')

    #allow cross-domain request
    CORS(app, resources={"/api" + "/*": {"origins": "*"}})

    #set static folder
    if not os.path.exists(app.config['STATICDIR']):
        os.makedirs(app.config['STATICDIR'], exist_ok=True)
    app.static_folder = app.config['STATICDIR']

    #create database if not exist
    if not check_database():
        create_database()

    #modular application with Blueprint
    configure_blueprint(app, views.MODULES)

    configure_foundations(app)
    return app

def configure_blueprint(app: Flask, modules: List[Blueprint]) -> None:
    for module in modules:
        app.register_blueprint(module)

def configure_foundations(app: Flask) -> None:
    db.app = app
    db.init_app(app)

    with app.app_context():
        from app.models import Sample
        db.create_all()

    @app.before_request
    def before_request() -> None:
        g.ACCESSTIME = time.time()

    @app.after_request
    def after_request(response: Any) -> Any:
        logger.info("API_Execution_Time: %sms %s %s", int(round(time.time()-g.ACCESSTIME, 3)*1000), request.method, request.url)
        return response

    @app.errorhandler(Exception)
    def error_handler(e: Exception) -> jsonify:
        code = 500
        if isinstance(e, HTTPException):
            code = e.code
        return jsonify(error=str(e), code=code)

    @app.teardown_appcontext
    def shutdown_session(exception=None):
        db.session.remove()
