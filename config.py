import os

BASEDIR = os.path.abspath(os.path.dirname(__file__))
STATICDIR = os.path.join(BASEDIR, 'static')
LOGDIR = os.path.join(BASEDIR, 'logs')

DB_HOST = os.environ.get('DB_HOST') or 'db:3306'
DB_DATABASE = os.environ.get('DB_DATABASE')
DB_ROOT_PASSWORD = os.environ.get('DB_ROOT_PASSWORD')

SECRET_KEY = '2a26a648-3dfc-43b7-b057-bb2a368e9d8f'
SQLALCHEMY_TRACK_MODIFICATIONS = False
SQLALCHEMY_DATABASE_URI = 'mysql://{0}:{1}@{2}/{3}'.format('root', DB_ROOT_PASSWORD, DB_HOST, DB_DATABASE)
